//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException, NumberComparisonException {
        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
        // to see how IntelliJ IDEA suggests fixing it.
//        System.out.printf("Hello and welcome!");
//
//        for (int i = 1; i <= 5; i++) {
//            //TIP Press <shortcut actionId="Debug"/> to start debugging your code. We have set one <icon src="AllIcons.Debugger.Db_set_breakpoint"/> breakpoint
//            // for you, but you can always add more by pressing <shortcut actionId="ToggleLineBreakpoint"/>.
//            System.out.println("i = " + i);
//        }

//        Урок 7. Параметры методов.
//        Цель задания:
//        Совершенствование навыков работы с переменными, знакомство с
//        функциональными возможностями методов в java
//        Задание:
//        1.
//        Реализуйте метод, который находит из трех чисел то, которое делится на 2
//        остальных; или возвращает
//        -
//                1, если такого нет
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!");

        System.out.println(firstMethod(10,2,5));
        System.out.println(firstMethod(3,4,5));



//        2.
//        Реализуйте метод, который из двух массивов строк собирает третий, в
//        котором чередуются элементы первых двух

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!");
        String [] s1={"aaa","ccc","eee"};
        String [] s2={"bbb","ddd","fff"};

        String [] res=new String[0];
        res=secondMethod(res,s1,s2);

        for (int i=0;i<res.length;i++) System.out.println(res[i]);
//        3.
//        Возьмите любую программу, кот
//        орую вы писали до этого.. какая сердцу
//        ближе. Отрефакторите ее (улучшите читабельность кода)
//        -
//                путем разбиения
//        крупных частей на методы поменьше. Стало лучше? (надеюсь, да)

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!!!!!");
        //давно уже делаю сразу с рефакторингом все проги до этого через методы. так лучше конечно.
        // пример tema2_urok5

//        4.
//        Отрефакторите(улучшите читабельность) кода вашей реализации анализа
//        курса валют.

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!");

        //сделано tema2_urok3


//        5.
//        Р
//        еализуйте метод, который возвращает случайную цитату Уолтера Уайта.

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!!!!!!");
        getQuote();

        //        6.
//        Реализуйте метод, который выводит explanation сегодняшнего снимка дня
//                NASA

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!! 6 !!!!!!!!!!!!!!!!!!!!!");

        System.out.println(getExtNasaToday());



//        7.
//        Реализуйте метод, который возвращает explanation снимка дня NASA, в
//        качестве параметра принимайте LocalDate
//        -
//                дат
//        у, на которую нужно брать
//                снимок

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!! 7 !!!!!!!!!!!!!!!!!!!!!");
        System.out.println(" Введите дату в формате Год-месяц-день, например:");
        System.out.println("2024-01-20");
        Scanner scanner7=new Scanner(System.in);
        System.out.println(getExtNasaThis(scanner7.nextLine()));

//        8.
//        Реализуйте метод, который принимает два LocalDate, и сохраняет все
//        снимки дня NASA в указанный промежуток

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!! 8 !!!!!!!!!!!!!!!!!!!!!");

        System.out.println(" Введите две даты, с, по, в формате Год-месяц-день, например:");
        System.out.println("2024-01-19");
        System.out.println("2024-01-20");
        System.out.println("Дата С:");
        Scanner scanner8=new Scanner(System.in);
        String from = scanner8.nextLine();
        System.out.printf("Дата По:");
        String to = scanner8.nextLine();

        getExtNasaFromTo(from,to);
//        Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написана общая структура программы
//        3 балла
//                -
//                выполн
//        ено более 60% заданий, имеется не более 5 критичных
//                замечаний
//        4 балла
//                -
//                выполнено корректно более 80% технических заданий
//        5 баллов
//                -
//                все технические задания выполнены корректно, в полном объеме
//        Задание считается выполненным при условии, что слушатель полу
//        чил оценку не
//        менее 3 баллов
//

    }

   static public int firstMethod(int a, int b, int c){
       if (a%b==0&&a%c==0) return a;
       if (b%a==0&&b%c==0) return b;
       if (c%a==0&&c%b==0) return c;
       return 1;
   }


//   //по условию задания массивы на вход должны быть одинаковой длины. чтобы их элементы чередовались
    public static String[] secondMethod(String[] strings, String[] stra1, String[] stra2) {
        if (stra2.length!= stra1.length) {
            System.out.println("массивы строк должны быть одинаковой длины для работы с методом");
            return strings;
        }
        // Увеличиваем размер массива strings на сумму элементов stra1 и stra2
        strings = new String[strings.length + stra1.length + stra2.length];
        int index = 0;
        for (int i = 0; i < stra1.length; i++) {
            addStringToEnd(strings, stra1[i], index++);
            addStringToEnd(strings, stra2[i], index++);
        }

        return strings;
    }

    /** метод для задания 2
     * Добавляет указанную строку в конец массива строк.
     *
     * @param array Исходный массив строк.
     * @param str   Строка для добавления в конец массива.
     * @param index Индекс, куда будет добавлена строка.
     */
    public static void addStringToEnd(String[] array, String str, int index) {
        // Проверяем, достаточно ли места в массиве
        if (index >= array.length) {
            throw new IllegalArgumentException("Недостаточно места в массиве для добавления строки.");
        }
        // Добавляем новую строку в указанный индекс
        array[index] = str;
    }

 public static void getQuote() throws IOException {
     String page = downloadWebPage("https://favqs.com/api/qotd");
     System.out.println(page);
     int qStart=page.lastIndexOf("\",\"body\":\"");
     String quote= page.substring(qStart+10,page.length()-3);
     System.out.println("Цитата: "+quote);

     System.out.println("!!!!!!!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
     if (quote.length()>50){
         System.out.println(quote.substring(0,50)+"...");
     } else {
         System.out.println(quote);
     }

     System.out.println("!!!!!!!!!!!!!!!!!!!!!! 6 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
     System.out.println((quote.length()>50) ? quote.substring(0,50)+"..." : quote);


     System.out.println("!!!!!!!!!!!!!!!!!!!!!! 7 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
     int aStart=page.lastIndexOf("\"author_permalink\"");
     String authorQ=page.substring(aStart+20,qStart);
     System.out.println("Автор: ("+authorQ+")");

     System.out.println("!!!!!!!!!!!!!!!!!!!!!! 8 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
     System.out.println(!authorQ.equals("Wallter White")?"Здесь нет Wallter White, чего и следовало ожидать":"неужели и здесь есть цитата Wallter White, невероятно");


 }
    private static String downloadWebPage(String url) throws IOException {
        StringBuilder result = new StringBuilder();
        String line;
        URLConnection urlConnection = new URL(url).openConnection();
        try (InputStream is = urlConnection.getInputStream();
             BufferedReader br= new BufferedReader(new InputStreamReader(is))){
            while ((line=br.readLine())!=null){
                result.append(line);
            }
        }
        return result.toString();
    }



    static String getExtNasaToday() throws IOException {

        String myIpKey="b8BKYPTRSYvG6dKnwUbPAq8jht3kMxyrJ6WVBVRp";

            String page = downloadWebPage("https://api.nasa.gov/planetary/apod?api_key=" + myIpKey);


        StringBuilder strb7 =new StringBuilder(page);

            String startStr7="explanation\":\"";
            int start7=strb7.indexOf(startStr7)+startStr7.length();
            String endStr7="\",\"";
            int end7=strb7.indexOf(endStr7,start7);
            String subStr7=strb7.substring(start7,end7);

            return subStr7;

        }

    static String getExtNasaThis(String LocalDate) throws IOException {

        String myIpKey="b8BKYPTRSYvG6dKnwUbPAq8jht3kMxyrJ6WVBVRp";


        String page = downloadWebPage("https://api.nasa.gov/planetary/apod?api_key=" + myIpKey + "&start_date=" + LocalDate + "&end_date=" + LocalDate);
        StringBuilder strb7 =new StringBuilder(page);

        String startStr7="explanation\":\"";
        int start7=strb7.indexOf(startStr7)+startStr7.length();
        String endStr7="\",\"";
        int end7=strb7.indexOf(endStr7,start7);
        String subStr7=strb7.substring(start7,end7);

        return subStr7;

    }

    static void getExtNasaFromTo(String from, String to) throws IOException, NumberComparisonException {

        String myIpKey="b8BKYPTRSYvG6dKnwUbPAq8jht3kMxyrJ6WVBVRp";


        String page = downloadWebPage("https://api.nasa.gov/planetary/apod?api_key=" + myIpKey + "&start_date=" + from + "&end_date=" + to);
        StringBuilder strb7 =new StringBuilder(page);

        //Нужно сделать проверку чтобы from была меньше to
        //т.е. строка парсится на год месяц и день
        //год месяц и день to должен быть больше или равен from
        //метод получения массива строк всех дат с указанной form по to

       if (validDateArray(toArrSplitData(from),toArrSplitData(to))) {
           String[] data = generateAndSortDateRange(from, to);


           for (String d : data){

           String pageM = downloadWebPage("https://api.nasa.gov/planetary/apod?api_key=" + myIpKey + "&start_date=" + d + "&end_date=" + d);
//            System.out.println(pageM);

           StringBuilder strb8 = new StringBuilder(pageM);

           String startStrUrl7 = "hdurl\":\"";
           String endStrUrl7 = "\",\"";
           String subUrlstr7 = new String();
           int startUrl7;
           int endUrl7;
           try {
               //тут бывает что не изображение а видео поэтому другой формат парсинга. но если неудача пропускаем итерацию
               startUrl7 = strb8.indexOf(startStrUrl7) + startStrUrl7.length();
               endUrl7 = strb8.indexOf(endStrUrl7, startUrl7);
           } catch (Exception e) {
               System.out.println(e);
               continue;
           }
           ;
           //сюда попадаем если не попадаем в catch значит у нас есть ссылка
           subUrlstr7 = strb8.substring(startUrl7, endUrl7);

           //если не вышли из цикла сохраняем файл изображения
           //получаем имя первого файла из ссылки
           StringBuilder file = new StringBuilder();
           String[] arrFn = subUrlstr7.split("/");
           file.delete(0, file.length());
           file.append(arrFn[arrFn.length - 1]);


           downloadImage(subUrlstr7, "files/" + file.toString());
       }
        }

        }

    //Метод скачивания изображения по url
    static void downloadImage(String url, String fullname) throws IOException{
        try(InputStream in = new URL(url).openStream()){
            Files.copy(in, Paths.get(fullname));
        } catch (Exception e) {System.out.println(e);}
    }


    //Нужно сделать проверку чтобы from была меньше to
    //т.е. строка парсится на год месяц и день
    //год месяц и день to должен быть больше или равен from
    //метод получения массива строк всех дат с указанной form по to

  static boolean validDateArray(String[] frsp, String[] tosp) throws NumberComparisonException {
        if ((Integer.parseInt(frsp[0])<Integer.parseInt(tosp[0]))&&(Integer.parseInt(frsp[1])<Integer.parseInt(tosp[1]))&&(Integer.parseInt(frsp[2])<Integer.parseInt(tosp[2])))  {
             System.out.println("дата первого параметре from должна быть старше, даты второго параметра to");
      return false;
            //     throw new NumberComparisonException("Дата от не должна быть меньше даты до");
        }
    return true;
  }

  static String[] toArrSplitData(String str){
      return str.split("-");
  }


    public class NumberComparisonException extends Exception {
        public NumberComparisonException(String message) {
            super(message);
        }
    }

    public static String[] generateAndSortDateRange(String startDateStr, String endDateStr) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate start = LocalDate.parse(startDateStr, formatter);
        LocalDate end = LocalDate.parse(endDateStr, formatter);

        List<String> dates = new ArrayList<>();
        while (!start.isAfter(end)) {
            dates.add(start.format(formatter));
            start = start.plusDays(1);
        }

        // Преобразование списка в массив
        String[] sortedDatesArray = dates.toArray(new String[0]);

        // Сортировка массива дат
        // Использование @FunctionalInterface для обхода ограничений JDK
        Comparator<String> comparator = new Comparator<String>() {
            private final DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE;

            @Override
            public int compare(String o1, String o2) {
                return LocalDate.parse(o1, formatter).compareTo(LocalDate.parse(o2, formatter));
            }
        };

        Arrays.sort(sortedDatesArray, comparator);


        return sortedDatesArray;
    }
}